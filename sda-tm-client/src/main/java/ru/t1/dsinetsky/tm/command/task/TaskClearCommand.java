package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskClearRequest;
import ru.t1.dsinetsky.tm.exception.GeneralException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_CLEAR;

    @NotNull
    public static final String DESCRIPTION = "Clear all tasks";

    @Override
    public void execute() throws GeneralException {
        getTaskEndpoint().clearTask(new TaskClearRequest(getToken()));
        System.out.println("Tasks successfully cleared!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
