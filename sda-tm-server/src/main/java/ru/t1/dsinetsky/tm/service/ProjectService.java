package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.User;

import java.util.Random;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IProjectRepository repository) {
        super(repository);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return repository.create(userId, name);
    }

    @Override
    @NotNull
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return repository.create(userId, name, description);
    }

    @Override
    @NotNull
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (!existsById(userId, id)) throw new ProjectNotFoundException();
        @NotNull final Project project = findById(userId, id);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    @NotNull
    public Project updateByIndex(@Nullable final String userId, final int index, @Nullable final String name, @Nullable final String description) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        @NotNull final Project project = findByIndex(userId, index);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    @NotNull
    public Project changeStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        if (status == null) throw new InvalidStatusException();
        if (!existsById(userId, id)) throw new ProjectNotFoundException();
        @NotNull final Project project = findById(userId, id);
        project.setStatus(status);
        return project;
    }

    @Override
    @NotNull
    public Project changeStatusByIndex(@Nullable final String userId, final int index, @Nullable final Status status) throws GeneralException {
        if (userId == null || userId.isEmpty()) throw new UserNotLoggedException();
        if (index < 0) throw new NegativeIndexException();
        if (index >= getSize(userId)) throw new IndexOutOfSizeException(getSize(userId));
        if (status == null) throw new InvalidStatusException();
        @NotNull final Project project = findByIndex(userId, index);
        project.setStatus(status);
        return project;
    }

    @Override
    public void createTest(@NotNull final User user) throws GeneralException {
        @NotNull final Random randomizer = new Random();
        final int count = 10;
        @NotNull final String name = user.getLogin() + "-project-";
        @NotNull final String description = "Project for " + user.getLogin() + " number ";
        for (int i = 1; i < count; i++) {
            @NotNull final Project project = create(user.getId(), name + randomizer.nextInt(11), description + i);
            @Nullable final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            project.setStatus(status);
        }
    }

}
