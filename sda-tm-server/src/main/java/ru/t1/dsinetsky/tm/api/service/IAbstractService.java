package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.List;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {

    @NotNull
    List<M> returnAll(@Nullable Sort sort);

}
