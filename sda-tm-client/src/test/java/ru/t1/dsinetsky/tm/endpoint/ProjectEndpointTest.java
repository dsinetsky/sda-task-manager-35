package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.dto.request.project.*;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.response.project.*;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.marker.IntegrationCategory;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.service.PropertyService;

import java.util.UUID;

@Category(IntegrationCategory.class)
public class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String userPassword = "Test";

    @NotNull
    private final String projectName = "New project";

    @NotNull
    private final String projectDescription = "New project";

    @NotNull
    private final String projectNameUpdate = "Update project";

    @NotNull
    private final String projectDescriptionUpdate = "Update project";

    @NotNull
    private final Status newStatus = Status.IN_PROGRESS;

    @NotNull
    private final Sort sort = Sort.BY_STATUS;

    @Nullable
    private String testToken;

    @NotNull
    private final String invalidToken = UUID.randomUUID().toString();

    @NotNull
    private final String invalidProjectId = UUID.randomUUID().toString();

    @Before
    public void before() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @After
    public void after() {
        projectEndpoint.clearProject(new ProjectClearRequest(testToken));
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void createProjectPositiveTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName(projectName);
        request.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertNotNull(response);
        @Nullable final Project project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDesc());
    }

    @Test
    public void createProjectNullNameTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName(null);
        request.setDescription(projectDescription);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void createProjectEmptyNameTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName("");
        request.setDescription(projectDescription);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void createProjectNullDescriptionTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName(projectName);
        request.setDescription(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void createProjectEmptyDescriptionTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(testToken);
        request.setName(projectName);
        request.setDescription("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void createProjectEmptyTokenTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setName(projectName);
        request.setDescription(projectDescription);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void createProjectInvalidTokenTest() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(invalidToken);
        request.setName(projectName);
        request.setDescription(projectDescription);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.createProject(request));
    }

    @Test
    public void clearProjectPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectClearResponse response = projectEndpoint.clearProject(new ProjectClearRequest(testToken));
        Assert.assertNotNull(response);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        Assert.assertNull(projectListResponse.getProjects());
    }

    @Test
    public void clearProjectEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(new ProjectClearRequest()));
    }

    @Test
    public void clearProjectInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.clearProject(new ProjectClearRequest(invalidToken)));
    }

    @Test
    public void changeProjectStatusByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setId(project.getId());
        request.setStatus(newStatus);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(newStatus, updatedProject.getStatus());
    }

    @Test
    public void changeProjectStatusByIdNullProjectIdTest() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setId(null);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIdEmptyProjectIdTest() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setId("");
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIdInvalidProjectIdTest() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setId(invalidProjectId);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIdNullStatusTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(testToken);
        request.setId(project.getId());
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIdEmptyTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest();
        request.setId(project.getId());
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIdInvalidTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(invalidToken);
        request.setId(project.getId());
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void changeProjectStatusByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(testToken);
        request.setIndex(size - 1);
        request.setStatus(newStatus);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(newStatus, updatedProject.getStatus());
    }

    @Test
    public void changeProjectStatusByIndexNullStatusTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(testToken);
        request.setIndex(size - 1);
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(request));
    }

    @Test
    public void changeProjectStatusByIndexNegativeIndexTest() {
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(testToken);
        request.setIndex(-1);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(request));
    }

    @Test
    public void changeProjectStatusByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(testToken);
        request.setIndex(size);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(request));
    }

    @Test
    public void changeProjectStatusByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest()));
    }

    @Test
    public void changeProjectStatusByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusByIndex(new ProjectChangeStatusByIndexRequest(invalidToken)));
    }

    @Test
    public void completeProjectByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final ProjectCompleteByIndexResponse response = projectEndpoint.completeProjectByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(Status.COMPLETED, updatedProject.getStatus());
    }

    @Test
    public void completeProjectByIndexNegativeIndexTest() {
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectByIndex(request));
    }

    @Test
    public void completeProjectByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectByIndex(request));
    }

    @Test
    public void completeProjectByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectByIndex(new ProjectCompleteByIndexRequest()));
    }

    @Test
    public void completeProjectByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectByIndex(new ProjectCompleteByIndexRequest(invalidToken)));
    }

    @Test
    public void startProjectByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final ProjectStartByIndexResponse response = projectEndpoint.startProjectByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(Status.IN_PROGRESS, updatedProject.getStatus());
    }

    @Test
    public void startProjectByIndexNegativeIndexTest() {
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(request));
    }

    @Test
    public void startProjectByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectStartByIndexRequest request = new ProjectStartByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(request));
    }

    @Test
    public void startProjectByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest()));
    }

    @Test
    public void startProjectByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectByIndex(new ProjectStartByIndexRequest(invalidToken)));
    }

    @Test
    public void completeProjectByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(testToken);
        request.setId(project.getId());
        @NotNull final ProjectCompleteByIdResponse response = projectEndpoint.completeProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(Status.COMPLETED, updatedProject.getStatus());
    }

    @Test
    public void completeProjectByIdNullProjectIdTest() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void completeProjectByIdEmptyProjectIdTest() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void completeProjectByIdInvalidProjectIdTest() {
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(testToken);
        request.setId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void completeProjectByIdEmptyTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest();
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void completeProjectByIdInvalidTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(invalidToken);
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.completeProjectById(request));
    }

    @Test
    public void startProjectByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(testToken);
        request.setId(project.getId());
        @NotNull final ProjectStartByIdResponse response = projectEndpoint.startProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(Status.IN_PROGRESS, updatedProject.getStatus());
    }

    @Test
    public void startProjectByIdNullProjectIdTest() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void startProjectByIdEmptyProjectIdTest() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void startProjectByIdInvalidProjectIdTest() {
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(testToken);
        request.setId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void startProjectByIdEmptyTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest();
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void startProjectByIdInvalidTokenTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectStartByIdRequest request = new ProjectStartByIdRequest(invalidToken);
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.startProjectById(request));
    }

    @Test
    public void findProjectByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectFindByIdRequest request = new ProjectFindByIdRequest(testToken);
        request.setId(project.getId());
        @NotNull final ProjectFindByIdResponse response = projectEndpoint.findProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project foundProject = response.getProject();
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project, foundProject);
        Assert.assertEquals(project.getId(), foundProject.getId());
    }

    @Test
    public void findProjectByIdNullProjectIdTest() {
        @NotNull final ProjectFindByIdRequest request = new ProjectFindByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(request));
    }

    @Test
    public void findProjectByIdEmptyProjectIdTest() {
        @NotNull final ProjectFindByIdRequest request = new ProjectFindByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(request));
    }

    @Test
    public void findProjectByIdInvalidProjectIdTest() {
        @NotNull final ProjectFindByIdRequest request = new ProjectFindByIdRequest(testToken);
        request.setId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(request));
    }

    @Test
    public void findProjectByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(new ProjectFindByIdRequest()));
    }

    @Test
    public void findProjectByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(new ProjectFindByIdRequest(invalidToken)));
    }

    @Test
    public void findProjectByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectFindByIndexRequest request = new ProjectFindByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final ProjectFindByIndexResponse response = projectEndpoint.findProjectByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project foundProject = response.getProject();
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(project, foundProject);
        Assert.assertEquals(project.getId(), foundProject.getId());
    }

    @Test
    public void findProjectByIndexNegativeIndexTest() {
        @NotNull final ProjectFindByIndexRequest request = new ProjectFindByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectByIndex(request));
    }

    @Test
    public void findProjectByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectFindByIndexRequest request = new ProjectFindByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectByIndex(request));
    }

    @Test
    public void findProjectByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectByIndex(new ProjectFindByIndexRequest()));
    }

    @Test
    public void findProjectByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectByIndex(new ProjectFindByIndexRequest(invalidToken)));
    }

    @Test
    public void listProjectTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        Assert.assertNotNull(projectListResponse);
        Assert.assertFalse(projectListResponse.getProjects().isEmpty());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest()));
        Assert.assertThrows(Exception.class, () -> projectEndpoint.listProject(new ProjectListRequest(invalidToken)));
    }

    @Test
    public void listProjectSortPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListRequest request = new ProjectListRequest(testToken);
        request.setSort(sort);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(request);
        Assert.assertNotNull(projectListResponse);
        Assert.assertFalse(projectListResponse.getProjects().isEmpty());
    }

    @Test
    public void listProjectNullSortTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListRequest request = new ProjectListRequest(testToken);
        request.setSort(null);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(request);
        Assert.assertNotNull(projectListResponse);
        Assert.assertFalse(projectListResponse.getProjects().isEmpty());
    }

    @Test
    public void removeProjectByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(testToken);
        request.setId(project.getId());
        @NotNull final ProjectRemoveByIdResponse response = projectEndpoint.removeProjectById(request);
        Assert.assertNotNull(response);
        @NotNull final ProjectFindByIdRequest projectFindByIdRequest = new ProjectFindByIdRequest(testToken);
        projectFindByIdRequest.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(projectFindByIdRequest));
    }

    @Test
    public void removeProjectByIdNullProjectIdTest() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(request));
    }

    @Test
    public void removeProjectByIdEmptyProjectIdTest() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(request));
    }

    @Test
    public void removeProjectByIdInvalidProjectIdTest() {
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(testToken);
        request.setId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(request));
    }

    @Test
    public void removeProjectByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest()));
    }

    @Test
    public void removeProjectByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(invalidToken)));
    }

    @Test
    public void removeProjectByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final ProjectRemoveByIndexResponse response = projectEndpoint.removeProjectByIndex(request);
        Assert.assertNotNull(response);
        @NotNull final ProjectFindByIdRequest projectFindByIdRequest = new ProjectFindByIdRequest(testToken);
        projectFindByIdRequest.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.findProjectById(projectFindByIdRequest));
    }

    @Test
    public void removeProjectByIndexNegativeIndexTest() {
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(request));
    }

    @Test
    public void removeProjectByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(request));
    }

    @Test
    public void removeProjectByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest()));
    }

    @Test
    public void removeProjectByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectByIndex(new ProjectRemoveByIndexRequest(invalidToken)));
    }

    @Test
    public void updateProjectByIdPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setId(project.getId());
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(project.getId(), updatedProject.getId());
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals(projectDescriptionUpdate, updatedProject.getDesc());
    }

    @Test
    public void updateProjectByIdEmptyDescriptionTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(null);
        request.setId(project.getId());
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(project.getId(), updatedProject.getId());
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals("", updatedProject.getDesc());
    }

    @Test
    public void updateProjectByIdNullNameTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(null);
        request.setDescription(projectDescriptionUpdate);
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void updateProjectByIdEmptyNameTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        @Nullable final Project project = projectResponse.getProject();
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName("");
        request.setDescription(projectDescriptionUpdate);
        request.setId(project.getId());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void updateProjectByIdNullProjectIdTest() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void updateProjectByIdEmptyProjectIdTest() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void updateProjectByIdInvalidProjectIdTest() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
    }

    @Test
    public void updateProjectByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest()));
    }

    @Test
    public void updateProjectByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(new ProjectUpdateByIdRequest(invalidToken)));
    }

    @Test
    public void updateProjectByIndexPositiveTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setIndex(size - 1);
        @NotNull final ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals(projectDescriptionUpdate, updatedProject.getDesc());
    }

    @Test
    public void updateProjectByIndexEmptyDescriptionTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(null);
        request.setIndex(size - 1);
        @NotNull final ProjectUpdateByIndexResponse response = projectEndpoint.updateProjectByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Project updatedProject = response.getProject();
        Assert.assertNotNull(updatedProject);
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals("", updatedProject.getDesc());
    }

    @Test
    public void updateProjectByIndexNullNameTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @NotNull final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName(null);
        request.setDescription(projectDescriptionUpdate);
        request.setIndex(size - 1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(request));
    }

    @Test
    public void updateProjectByIndexEmptyNameTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName("");
        request.setDescription(projectDescriptionUpdate);
        request.setIndex(size - 1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(request));
    }

    @Test
    public void updateProjectByIndexNegativeIndexTest() {
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(request));
    }

    @Test
    public void updateProjectByIndexOutOfBoundIndexTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        projectEndpoint.createProject(projectRequest);
        @Nullable final ProjectListResponse projectListResponse = projectEndpoint.listProject(new ProjectListRequest(testToken));
        final int size = projectListResponse.getProjects().size();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(testToken);
        request.setName(projectNameUpdate);
        request.setDescription(projectDescriptionUpdate);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(request));
    }

    @Test
    public void updateProjectByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest()));
    }

    @Test
    public void updateProjectByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectByIndex(new ProjectUpdateByIndexRequest(invalidToken)));
    }

}
