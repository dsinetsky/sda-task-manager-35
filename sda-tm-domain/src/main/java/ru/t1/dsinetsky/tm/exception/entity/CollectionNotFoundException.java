package ru.t1.dsinetsky.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

public final class CollectionNotFoundException extends GeneralEntityException {

    public CollectionNotFoundException(@NotNull String modelType) {
        super("List of " + modelType + " is empty!");
    }

}
