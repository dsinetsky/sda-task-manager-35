package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    public void showProject(@Nullable final Project project) {
        if (project != null) {
            System.out.println("Id: " + project.getId());
            System.out.println("Name: " + project.getName());
            System.out.println("Desc: " + project.getDesc());
            System.out.println("Status: " + Status.toName(project.getStatus()));
        }
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
