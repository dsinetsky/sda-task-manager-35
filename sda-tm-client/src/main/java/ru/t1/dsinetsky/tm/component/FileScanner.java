package ru.t1.dsinetsky.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.exception.GeneralException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class FileScanner {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final String directoryPath = "./execute/";

    @NotNull
    private final File folder = new File(directoryPath);

    public FileScanner(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void process() {
        for (@NotNull final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.terminalRun(fileName);
                } catch (@NotNull final Exception e) {
                    bootstrap.getLoggerService().error(e);
                }
            }
        }
    }

    public void init() {
        if (!folder.exists() && !folder.isDirectory()) folder.mkdir();
        bootstrap.getCommandService().getArgumentCommands()
                .stream()
                .map(AbstractCommand::getName)
                .forEach(commands::add);
        executorService.scheduleWithFixedDelay(this::process, 0, 5, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

}
