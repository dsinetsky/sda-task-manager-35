package ru.t1.dsinetsky.tm.command.user.admin;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dsinetsky.tm.command.user.AbstractUserCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;

public abstract class AbstractAdminCommand extends AbstractUserCommand {

    @NotNull
    protected IAdminEndpoint getAdminEndpoint() {
        return getServiceLocator().getAdminEndpoint();
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
