package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractRepository;
import ru.t1.dsinetsky.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    protected final List<M> models = new ArrayList<>();

    @Override
    @NotNull
    public List<M> returnAll() {
        return models;
    }

    @Override
    @NotNull
    public List<M> returnAll(@Nullable final Comparator comparator) {
        @NotNull final List<M> sortedList = new ArrayList<>(models);
        sortedList.sort(comparator);
        return sortedList;
    }

    @Override
    @Nullable
    public M add(@NotNull final M model) {
        models.add(model);
        return model;
    }

    @Override
    @Nullable
    public Collection<M> add(@NotNull final Collection<M> models) {
        this.models.addAll(models);
        return models;
    }

    @Override
    @Nullable
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    @Nullable
    public M findById(@NotNull final String id) {
        return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findByIndex(final int index) {
        if (index < 0) return null;
        if (index >= models.size()) return null;
        return models.get(index);
    }

    @Override
    @Nullable
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findById(id);
        if (model == null) return null;
        else models.remove(model);
        return model;
    }

    @Override
    @Nullable
    public M removeByIndex(final int index) {
        @Nullable final M model = findByIndex(index);
        if (model == null) return null;
        else models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return models
                .stream()
                .anyMatch(m -> id.equals(m.getId()));
    }

    @Override
    public int getSize() {
        return (int) models
                .stream()
                .count();
    }

}
