package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;
import ru.t1.dsinetsky.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskListRequest(final @Nullable String token) {
        super(token);
    }

}
