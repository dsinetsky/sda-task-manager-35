package ru.t1.dsinetsky.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IAuthService;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.ISessionService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.CryptUtil;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.Date;
import java.util.Objects;


public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(@NotNull final IUserService userService,
                       @NotNull final IPropertyService propertyService,
                       @NotNull final ISessionService sessionService) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public User registry(@NotNull final String login, @NotNull final String password) throws GeneralException {
        return userService.add(UserBuilder.create().login(login).password(password).toUser());
    }

    private Session createSession(@NotNull final User user) throws GeneralException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        sessionService.add(session);
        return session;
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final Session session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private String getToken(@NotNull final User user) throws GeneralException {
        return getToken(createSession(user));
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new IncorrectLoginPasswordException();
        @NotNull final User user = userService.findUserByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!Objects.equals(hash, user.getPasswordHash())) throw new IncorrectLoginPasswordException();
        return getToken(user);
    }

    @Override
    public void logout(@Nullable final String token) throws GeneralException {
        @NotNull final Session session = validateToken(token);
        sessionService.removeById(session.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(@Nullable final String token) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull Session session = objectMapper.readValue(json, Session.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @NotNull
    @Override
    public User check(@Nullable final String login, @Nullable final String password) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new IncorrectLoginPasswordException();
        @NotNull final User user = userService.findUserByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!Objects.equals(hash, user.getPasswordHash())) throw new IncorrectLoginPasswordException();
        return user;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = userService.findUserByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!userService.isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = userService.findUserByLogin(login);
        user.setLocked(false);
    }

}
