package ru.t1.dsinetsky.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(@Nullable IPropertyService propertyService, @Nullable final String value) {
        if (value == null) return null;
        if (propertyService == null) return null;
        @Nullable final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5Hash(secret + result + secret);
        }
        return result;
    }

    @Nullable
    static String md5Hash(@Nullable final String value) {
        if (value == null) return null;
        try {
            @NotNull final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            final byte[] array = messageDigest.digest(value.getBytes());
            @NotNull final StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                stringBuffer.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
