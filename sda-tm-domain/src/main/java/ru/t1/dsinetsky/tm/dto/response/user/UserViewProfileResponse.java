package ru.t1.dsinetsky.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.response.AbstractResultResponse;
import ru.t1.dsinetsky.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public final class UserViewProfileResponse extends AbstractResultResponse {

    @Nullable
    private User user;

    public UserViewProfileResponse(@Nullable final User user) {
        this.user = user;
    }

    public UserViewProfileResponse(final @NotNull Throwable throwable) {
        super(throwable);
    }

}
