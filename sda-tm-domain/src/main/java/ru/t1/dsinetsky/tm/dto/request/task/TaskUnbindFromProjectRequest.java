package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskUnbindFromProjectRequest(final @Nullable String token) {
        super(token);
    }

}
