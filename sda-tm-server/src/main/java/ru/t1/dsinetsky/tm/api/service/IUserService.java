package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.GeneralUserException;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IUserService extends IAbstractService<User> {

    @NotNull
    User add(@NotNull User newUser) throws GeneralException;

    void removeUserByLogin(String login) throws GeneralException;

    @NotNull
    User findUserByLogin(String login) throws GeneralUserException;

    @NotNull
    User updateUserById(String id, String firstName, String lastName, String middleName) throws GeneralException;

    @NotNull
    User updateUserByLogin(String login, String firstName, String lastName, String middleName) throws GeneralUserException;

    @NotNull
    User updateEmailById(String id, String email) throws GeneralException;

    @NotNull
    User updateEmailByLogin(String login, String email) throws GeneralUserException;

    @NotNull
    User changePassword(String id, String password) throws GeneralException;

    @NotNull
    List<User> returnAll();

    boolean isUserExistByLogin(String login) throws GeneralUserException;

    void createTest() throws GeneralException;

}
