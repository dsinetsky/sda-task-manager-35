package ru.t1.dsinetsky.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.marker.UnitCategory;
import ru.t1.dsinetsky.tm.model.User;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String userPassword = "Test";

    @After
    public void after() throws GeneralException {
        userRepository.clear();
    }

    @Test
    @SneakyThrows
    public void addPositiveTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        final int beforeSize = userRepository.getSize();
        @Nullable final User addedUser = userRepository.add(newUser);
        final int difference = userRepository.getSize() - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertNotNull(addedUser);
        Assert.assertEquals(addedUser.getId(), newUser.getId());
        Assert.assertEquals(addedUser, newUser);
    }

    @Test
    @SneakyThrows
    public void removeByLoginTest() {
        @NotNull final User user = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(user);
        final int beforeSize = userRepository.getSize();
        userRepository.removeByLogin(userLogin);
        final int difference = beforeSize - userRepository.getSize();
        Assert.assertEquals(1, difference);
        Assert.assertFalse(userRepository.isUserLoginExist(userLogin));
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        @NotNull final User user = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(user);
        userRepository.clear();
        Assert.assertEquals(0, userRepository.returnAll().size());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(newUser);
        @NotNull final String userId = newUser.getId();
        final int beforeSize = userRepository.getSize();
        @Nullable final User foundUser = userRepository.findById(userId);
        Assert.assertEquals(beforeSize, userRepository.getSize());
        Assert.assertNotNull(foundUser);
        Assert.assertEquals(foundUser.getId(), userId);
        Assert.assertEquals(foundUser, newUser);
    }

    @Test
    @SneakyThrows
    public void findUserByLoginTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(newUser);
        final int beforeSize = userRepository.getSize();
        @Nullable final User foundUser = userRepository.findUserByLogin(userLogin);
        Assert.assertEquals(beforeSize, userRepository.getSize());
        Assert.assertNotNull(foundUser);
        Assert.assertEquals(foundUser.getId(), newUser.getId());
        Assert.assertEquals(foundUser, newUser);
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(newUser);
        Assert.assertTrue(userRepository.existsById(newUser.getId()));
    }

    @Test
    @SneakyThrows
    public void isUserExistByLoginTest() {
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(newUser);
        Assert.assertTrue(userRepository.isUserLoginExist(newUser.getLogin()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        userRepository.clear();
        @NotNull final User newUser = UserBuilder.create().login(userLogin).password(userPassword).toUser();
        userRepository.add(newUser);
        Assert.assertEquals(1, userRepository.getSize());
    }

}
