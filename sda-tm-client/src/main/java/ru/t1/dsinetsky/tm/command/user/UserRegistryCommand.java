package ru.t1.dsinetsky.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_REGISTRY_USER;

    @NotNull
    public static final String DESCRIPTION = "Registry user in system";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter login:");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("Enter password:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        getUserEndpoint().registryUser(request);
        System.out.println("User successfully registered!");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
