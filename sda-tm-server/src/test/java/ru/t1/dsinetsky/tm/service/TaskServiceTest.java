package ru.t1.dsinetsky.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.marker.UnitCategory;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String taskName = "New Task";

    @NotNull
    private final String projectName = "New Project";

    @NotNull
    private final String taskNameUpdate = "Updated task";

    @NotNull
    private final String taskDescription = "New Task's description";

    @NotNull
    private final String projectDescription = "New Project's description";

    @NotNull
    private final Project project = new Project(projectName, projectDescription);

    @NotNull
    private final String taskDescriptionUpdate = "Updated task description";

    @NotNull
    private final Status newStatus = Status.IN_PROGRESS;

    @NotNull
    private final List<String> tasksListName = new ArrayList<>(Arrays.asList("TestTask1", "TestTask3", "TestTask2"));

    @NotNull
    private final List<Task> expectedTaskList = new ArrayList<>();

    @NotNull
    private final Sort newSort = Sort.BY_NAME;

    {
        for (@NotNull final String name : tasksListName) {
            @NotNull Task task = new Task(name);
            task.setProjectId(project.getId());
            expectedTaskList.add(task);
        }
    }

    @After
    public void after() throws GeneralException {
        projectRepository.clear();
        taskService.clear();
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.create(null, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.create("", taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.create(userId, null));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.create(userId, ""));
        Assert.assertThrows(DescIsEmptyException.class, () -> taskService.create(userId, taskName, null));
        Assert.assertThrows(DescIsEmptyException.class, () -> taskService.create(userId, taskName, ""));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task newTask = taskService.create(userId, taskName, taskDescription);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(taskName, newTask.getName());
        Assert.assertEquals(taskDescription, newTask.getDesc());
    }

    @Test
    @SneakyThrows
    public void createWithoutDescriptionTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.create(null, taskNameUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.create("", taskNameUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.create(userId, null));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.create(userId, ""));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task newTask = taskService.create(userId, taskName);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(userId, newTask.getUserId());
        Assert.assertEquals(taskName, newTask.getName());
        Assert.assertTrue(newTask.getDesc().isEmpty());
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> taskService.updateById(userId, null, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> taskService.updateById(userId, "", taskNameUpdate, taskDescriptionUpdate));
        @NotNull final Task newTask = new Task(taskName, taskDescription);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.updateById(null, taskId, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.updateById("", taskId, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.updateById(userId, taskId, null, taskDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.updateById(userId, taskId, "", taskDescriptionUpdate));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task updatedTask = taskService.updateById(userId, taskId, taskNameUpdate, taskDescriptionUpdate);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals(taskDescriptionUpdate, updatedTask.getDesc());
    }

    @Test
    @SneakyThrows
    public void updateByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> taskService.updateByIndex(userId, -1, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> taskService.updateByIndex(userId, taskService.getSize(userId), taskNameUpdate, taskDescriptionUpdate));
        @NotNull final Task newTask = new Task(taskName, taskDescription);
        taskService.add(userId, newTask);
        final int beforeSize = taskService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.updateByIndex(null, beforeSize - 1, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.updateByIndex("", beforeSize - 1, taskNameUpdate, taskDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.updateByIndex(userId, beforeSize - 1, null, taskDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> taskService.updateByIndex(userId, beforeSize - 1, "", taskDescriptionUpdate));
        @NotNull final Task updatedTask = taskService.updateByIndex(userId, beforeSize - 1, taskNameUpdate, taskDescriptionUpdate);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals(taskDescriptionUpdate, updatedTask.getDesc());
    }

    @Test
    @SneakyThrows
    public void changeStatusByIdTest() {
        @NotNull final Task newTask = new Task(taskName, taskDescription);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.changeStatusById(null, taskId, newStatus));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.changeStatusById("", taskId, newStatus));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> taskService.changeStatusById(userId, null, newStatus));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> taskService.changeStatusById(userId, "", newStatus));
        Assert.assertThrows(InvalidStatusException.class, () -> taskService.changeStatusById(userId, taskId, null));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task updatedTask = taskService.changeStatusById(userId, taskId, newStatus);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(newStatus, updatedTask.getStatus());
    }

    @Test
    @SneakyThrows
    public void changeStatusByIndexTest() {
        @NotNull final Task newTask = new Task(taskName, taskDescription);
        taskService.add(userId, newTask);
        final int beforeSize = taskService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.changeStatusByIndex(null, beforeSize - 1, newStatus));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.changeStatusByIndex("", beforeSize - 1, newStatus));
        Assert.assertThrows(NegativeIndexException.class, () -> taskService.changeStatusByIndex(userId, -1, newStatus));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> taskService.changeStatusByIndex(userId, beforeSize, newStatus));
        Assert.assertThrows(InvalidStatusException.class, () -> taskService.changeStatusByIndex(userId, beforeSize - 1, null));
        @NotNull final Task updatedTask = taskService.changeStatusByIndex(userId, beforeSize - 1, newStatus);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(newStatus, updatedTask.getStatus());
    }

    @Test
    @SneakyThrows
    public void returnAllTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.returnAll(""));
        taskService.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskService.create(userId, name);
        }
        @NotNull final List<Task> currentTaskList = taskService.returnAll(userId);
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

    @Test
    @SneakyThrows
    public void returnAllWithSortTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.returnAll("", newSort.getComparator()));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.returnAll(null, newSort.getComparator()));
        taskService.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskService.create(userId, name);
        }
        @NotNull final List<Task> currentTaskList = taskService.returnAll(userId, newSort);
        expectedTaskList.sort(newSort.getComparator());
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

    @Test
    @SneakyThrows
    public void addTest() {
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.add(userId, null));
        @NotNull final Task newTask = new Task(taskName);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.add("", newTask));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.add(null, newTask));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task addedTask = taskService.add(userId, newTask);
        final int difference = taskService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(addedTask.getId(), newTask.getId());
        Assert.assertEquals(addedTask, newTask);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.clear(""));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.clear(null));
        taskService.clear(userId);
        Assert.assertTrue(taskService.returnAll(userId).isEmpty());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> taskService.findById(userId, null));
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findById("", taskId));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findById(null, taskId));
        final int beforeSize = taskService.getSize(userId);
        @NotNull final Task foundTask = taskService.findById(userId, taskId);
        Assert.assertEquals(beforeSize, taskService.getSize(userId));
        Assert.assertEquals(foundTask.getId(), taskId);
        Assert.assertEquals(foundTask, newTask);
    }

    @Test
    @SneakyThrows
    public void findByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> taskService.findByIndex(userId, -1));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> taskService.findByIndex(userId, taskService.getSize(userId)));
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        final int beforeSize = taskService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findByIndex("", beforeSize - 1));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findByIndex(null, beforeSize - 1));
        @NotNull final Task foundTask = taskService.findByIndex(userId, beforeSize - 1);
        Assert.assertEquals(beforeSize, taskService.getSize(userId));
        Assert.assertEquals(foundTask.getId(), newTask.getId());
        Assert.assertEquals(foundTask, newTask);
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> taskService.removeById(userId, null));
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        final int beforeSize = taskService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.removeById("", taskId));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.removeById(null, taskId));
        @NotNull final Task removedTask = taskService.removeById(userId, taskId);
        final int difference = beforeSize - taskService.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.findById(userId, removedTask.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> taskService.removeByIndex(userId, -1));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> taskService.removeByIndex(userId, taskService.getSize(userId)));
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        final int beforeSize = taskService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findByIndex("", beforeSize - 1));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.findByIndex(null, beforeSize - 1));
        @NotNull final Task removedTask = taskService.removeByIndex(userId, beforeSize - 1);
        final int difference = beforeSize - taskService.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertThrows(EntityNotFoundException.class, () -> taskService.findById(userId, removedTask.getId()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.getSize(null));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.getSize(""));
        taskService.clear(userId);
        for (@NotNull final String name : tasksListName) {
            taskService.create(userId, name);
        }
        Assert.assertEquals(taskService.getSize(userId), expectedTaskList.size());
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> taskService.existsById(userId, null));
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> taskService.existsById(userId, ""));
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.existsById(null, taskId));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.existsById("", taskId));
        Assert.assertTrue(taskService.existsById(userId, taskId));
    }

    @Test
    @SneakyThrows
    public void returnTasksOfProjectTest() {
        @NotNull final String projectId = project.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.returnTasksOfProject(null, projectId));
        Assert.assertThrows(UserNotLoggedException.class, () -> taskService.returnTasksOfProject("", projectId));
        taskService.clear(userId);
        for (@NotNull final String name : tasksListName) {
            @NotNull Task task = new Task(name);
            task.setProjectId(projectId);
            taskService.add(userId, task);
        }
        @NotNull final List<Task> currentTaskList = taskService.returnTasksOfProject(userId, projectId);
        Assert.assertArrayEquals(currentTaskList.toArray(), expectedTaskList.toArray());
    }

    @Test
    @SneakyThrows
    public void bindProjectByIdTest() {
        @NotNull final Project project = new Project(projectName);
        projectRepository.add(userId, project);
        @NotNull final String projectId = project.getId();
        @NotNull final Task newTask = new Task(taskName);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectTaskService.bindProjectById(null, projectId, taskId));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectTaskService.bindProjectById("", projectId, taskId));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectTaskService.bindProjectById(userId, null, taskId));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectTaskService.bindProjectById(userId, "", taskId));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> projectTaskService.bindProjectById(userId, projectId, null));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> projectTaskService.bindProjectById(userId, projectId, ""));
        projectTaskService.bindProjectById(userId, projectId, taskId);
        Assert.assertEquals(newTask.getProjectId(), projectId);
    }

    @Test
    @SneakyThrows
    public void unbindProjectByIdTest() {
        @NotNull final Project project = new Project(projectName);
        projectRepository.add(userId, project);
        @NotNull final String projectId = project.getId();
        @NotNull final Task newTask = new Task(taskName);
        newTask.setProjectId(projectId);
        taskService.add(userId, newTask);
        @NotNull final String taskId = newTask.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectTaskService.unbindProjectById(null, projectId, taskId));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectTaskService.unbindProjectById("", projectId, taskId));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectTaskService.unbindProjectById(userId, null, taskId));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectTaskService.unbindProjectById(userId, "", taskId));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> projectTaskService.unbindProjectById(userId, projectId, null));
        Assert.assertThrows(TaskIdIsEmptyException.class, () -> projectTaskService.unbindProjectById(userId, projectId, ""));
        projectTaskService.unbindProjectById(userId, projectId, taskId);
        Assert.assertNull(newTask.getProjectId());
    }

}
