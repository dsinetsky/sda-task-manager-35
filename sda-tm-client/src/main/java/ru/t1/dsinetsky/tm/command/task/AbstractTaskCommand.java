package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dsinetsky.tm.command.AbstractCommand;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    protected void showListTask(@NotNull final List<Task> listTasks) {
        System.out.println("List of tasks:");
        int i = 1;
        for (final Task task : listTasks) {
            System.out.println(i + ". " + task.toString());
            i++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task != null) {
            System.out.println("Id: " + task.getId());
            System.out.println("Name: " + task.getName());
            System.out.println("Description: " + task.getDesc());
            System.out.println("Status: " + Status.toName(task.getStatus()));
        }
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
