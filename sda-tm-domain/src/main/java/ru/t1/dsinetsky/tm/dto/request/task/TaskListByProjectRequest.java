package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskListByProjectRequest(final @Nullable String token) {
        super(token);
    }

    public TaskListByProjectRequest(final @Nullable String token, @Nullable final String projectId) {
        super(token);
        this.projectId = projectId;
    }
}
