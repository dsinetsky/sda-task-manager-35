package ru.t1.dsinetsky.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectFindByIndexRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectFindByIndexResponse;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectFindByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_FIND_PROJECT_BY_INDEX;

    @NotNull
    public static final String DESCRIPTION = "Show project (if any) found by index";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        @NotNull final ProjectFindByIndexRequest request = new ProjectFindByIndexRequest(getToken());
        request.setIndex(index);
        @NotNull final ProjectFindByIndexResponse response = getProjectEndpoint().findProjectByIndex(request);
        @Nullable final Project project = response.getProject();
        showProject(project);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
