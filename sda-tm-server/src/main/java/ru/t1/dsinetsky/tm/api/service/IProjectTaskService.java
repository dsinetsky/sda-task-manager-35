package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

public interface IProjectTaskService {

    void bindProjectById(String userId, String projectId, String taskId) throws GeneralException;

    void unbindProjectById(String userId, String projectId, String taskId) throws GeneralException;

    @Nullable
    Project removeProjectById(@Nullable String userId, @Nullable String projectId) throws GeneralException;

    @NotNull
    Project removeProjectByIndex(@Nullable String userId, int index) throws GeneralException;

}
