package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskCreateRequest(final @Nullable String token) {
        super(token);
    }

}
