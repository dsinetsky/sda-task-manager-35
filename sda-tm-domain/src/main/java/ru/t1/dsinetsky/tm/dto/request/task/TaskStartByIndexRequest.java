package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskStartByIndexRequest extends AbstractUserRequest {

    private int index;

    public TaskStartByIndexRequest(final @Nullable String token) {
        super(token);
    }

    public TaskStartByIndexRequest(final @Nullable String token, final int index) {
        super(token);
        this.index = index;
    }

}
