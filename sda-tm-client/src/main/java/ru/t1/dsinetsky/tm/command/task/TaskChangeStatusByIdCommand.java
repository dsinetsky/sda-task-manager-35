package ru.t1.dsinetsky.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.dsinetsky.tm.dto.response.task.TaskChangeStatusByIdResponse;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID;

    @NotNull
    public static final String DESCRIPTION = "Changes status of task (if any) found by id";

    @Override
    public void execute() throws GeneralException {
        System.out.println("Enter id of task:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        @Nullable final String newStatus = TerminalUtil.nextLine();
        @NotNull final Status status = Status.toStatus(newStatus);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        @NotNull final TaskChangeStatusByIdResponse response = getTaskEndpoint().changeTaskStatusById(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
        System.out.println("Status successfully changed");
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}
