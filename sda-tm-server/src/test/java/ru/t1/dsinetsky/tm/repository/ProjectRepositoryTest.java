package ru.t1.dsinetsky.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = "New Project";

    @NotNull
    private final String projectDescription = "New Project's description";

    @NotNull
    private final List<String> projectsListName = new ArrayList<>(Arrays.asList("TestProject1", "TestProject3", "TestProject2"));

    @NotNull
    private final List<Project> expectedProjectList = new ArrayList<>();

    @NotNull
    private final Sort newSort = Sort.BY_NAME;

    {
        for (@NotNull final String name : projectsListName) {
            expectedProjectList.add(new Project(name));
        }
    }

    @After
    public void after() throws GeneralException {
        projectRepository.clear();
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        final int beforeSize = projectRepository.getSize(userId);
        @NotNull final Project newProject = projectRepository.create(userId, projectName, projectDescription);
        final int difference = projectRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(projectName, newProject.getName());
        Assert.assertEquals(projectDescription, newProject.getDesc());
    }

    @Test
    @SneakyThrows
    public void createWithoutDescriptionTest() {
        final int beforeSize = projectRepository.getSize(userId);
        @NotNull final Project newProject = projectRepository.create(userId, projectName);
        final int difference = projectRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectName, newProject.getName());
        Assert.assertTrue(newProject.getDesc().isEmpty());
    }

    @Test
    @SneakyThrows
    public void returnAllTest() {
        projectRepository.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectRepository.create(userId, name);
        }
        @NotNull final List<Project> currentProjectList = projectRepository.returnAll(userId);
        Assert.assertArrayEquals(currentProjectList.toArray(), expectedProjectList.toArray());
    }

    @Test
    @SneakyThrows
    public void returnAllWithSortTest() {
        projectRepository.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectRepository.create(userId, name);
        }
        @NotNull final List<Project> currentProjectList = projectRepository.returnAll(userId, newSort.getComparator());
        expectedProjectList.sort(newSort.getComparator());
        Assert.assertArrayEquals(currentProjectList.toArray(), expectedProjectList.toArray());
    }

    @Test
    @SneakyThrows
    public void addTest() {
        @NotNull final Project newProject = new Project(projectName);
        final int beforeSize = projectRepository.getSize(userId);
        @NotNull final Project addedProject = projectRepository.add(userId, newProject);
        final int difference = projectRepository.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(addedProject.getId(), newProject.getId());
        Assert.assertEquals(addedProject, newProject);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        projectRepository.clear(userId);
        Assert.assertTrue(projectRepository.returnAll(userId).isEmpty());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        @NotNull final Project newProject = new Project(projectName);
        projectRepository.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        final int beforeSize = projectRepository.getSize(userId);
        @Nullable final Project foundProject = projectRepository.findById(userId, projectId);
        Assert.assertEquals(beforeSize, projectRepository.getSize(userId));
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(foundProject.getId(), projectId);
        Assert.assertEquals(foundProject, newProject);
    }

    @Test
    @SneakyThrows
    public void findByIndexTest() {
        @NotNull final Project newProject = new Project(projectName);
        projectRepository.add(userId, newProject);
        final int beforeSize = projectRepository.getSize(userId);
        @Nullable final Project foundProject = projectRepository.findByIndex(userId, beforeSize - 1);
        Assert.assertEquals(beforeSize, projectRepository.getSize(userId));
        Assert.assertNotNull(foundProject);
        Assert.assertEquals(foundProject.getId(), newProject.getId());
        Assert.assertEquals(foundProject, newProject);
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        @NotNull final Project newProject = new Project(projectName);
        projectRepository.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        final int beforeSize = projectRepository.getSize(userId);
        projectRepository.removeById(userId, projectId);
        final int difference = beforeSize - projectRepository.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertFalse(projectRepository.existsById(userId, newProject.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        @NotNull final Project newProject = new Project(projectName);
        projectRepository.add(userId, newProject);
        final int beforeSize = projectRepository.getSize(userId);
        projectRepository.removeByIndex(userId, beforeSize - 1);
        final int difference = beforeSize - projectRepository.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertFalse(projectRepository.existsById(userId, newProject.getId()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        projectRepository.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectRepository.create(userId, name);
        }
        Assert.assertEquals(projectRepository.getSize(userId), expectedProjectList.size());
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        @NotNull final Project newProject = new Project(projectName);
        projectRepository.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        Assert.assertTrue(projectRepository.existsById(userId, projectId));
    }

}
