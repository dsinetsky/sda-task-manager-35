package ru.t1.dsinetsky.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.enumerated.Role;

import java.util.Objects;

@Getter
@Setter
public final class User extends AbstractModel {

    @NotNull
    private String passwordHash;

    @NotNull
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    private boolean locked = false;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Id: ").append(getId()).append("\n")
                .append("Login: ").append(login).append(" - ").append(role.getDisplayName()).append("\n")
                .append("Name: ").append(lastName).append(" ").append(firstName).append(" ").append(middleName).append("\n")
                .append("Email: ").append(email).toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final User user = (User) o;
        return passwordHash.equals(user.passwordHash) && login.equals(user.login) && Objects.equals(email, user.email) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(middleName, user.middleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(passwordHash, login, email, firstName, lastName, middleName);
    }

}
