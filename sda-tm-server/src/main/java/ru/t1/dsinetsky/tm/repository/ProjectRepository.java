package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    public Project create(@NotNull final String userId, @NotNull final String name) {
        return add(userId, new Project(name));
    }

    @Override
    @NotNull
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        return add(userId, new Project(name, description));
    }

}
