package ru.t1.dsinetsky.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    private int index;

    public ProjectStartByIndexRequest(final @Nullable String token) {
        super(token);
    }

    public ProjectStartByIndexRequest(final @Nullable String token, final int index) {
        super(token);
        this.index = index;
    }

}
