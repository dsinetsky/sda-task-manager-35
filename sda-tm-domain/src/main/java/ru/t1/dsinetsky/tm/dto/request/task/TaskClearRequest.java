package ru.t1.dsinetsky.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(final @Nullable String token) {
        super(token);
    }

}
