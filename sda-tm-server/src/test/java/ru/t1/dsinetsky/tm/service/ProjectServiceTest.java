package ru.t1.dsinetsky.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.EntityNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.exception.system.InvalidStatusException;
import ru.t1.dsinetsky.tm.exception.user.UserNotLoggedException;
import ru.t1.dsinetsky.tm.marker.UnitCategory;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = "New Project";

    @NotNull
    private final String projectNameUpdate = "Updated project";

    @NotNull
    private final String projectDescription = "New Project's description";

    @NotNull
    private final String projectDescriptionUpdate = "Updated project description";

    @NotNull
    private final Status newStatus = Status.IN_PROGRESS;

    @NotNull
    private final List<String> projectsListName = new ArrayList<>(Arrays.asList("TestProject1", "TestProject3", "TestProject2"));

    @NotNull
    private final List<Project> expectedProjectList = new ArrayList<>();

    @NotNull
    private final Sort newSort = Sort.BY_NAME;

    {
        for (@NotNull final String name : projectsListName) {
            expectedProjectList.add(new Project(name));
        }
    }

    @After
    public void after() throws GeneralException {
        projectService.clear();
    }

    @Test
    @SneakyThrows
    public void createWithDescriptionTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.create(null, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.create("", projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.create(userId, null));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.create(userId, ""));
        Assert.assertThrows(DescIsEmptyException.class, () -> projectService.create(userId, projectName, null));
        Assert.assertThrows(DescIsEmptyException.class, () -> projectService.create(userId, projectName, ""));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project newProject = projectService.create(userId, projectName, projectDescription);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(projectName, newProject.getName());
        Assert.assertEquals(projectDescription, newProject.getDesc());
    }

    @Test
    @SneakyThrows
    public void createWithoutDescriptionTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.create(null, projectNameUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.create("", projectNameUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.create(userId, null));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.create(userId, ""));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project newProject = projectService.create(userId, projectName);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectName, newProject.getName());
        Assert.assertTrue(newProject.getDesc().isEmpty());
    }

    @Test
    @SneakyThrows
    public void updateByIdTest() {
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectService.updateById(userId, null, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectService.updateById(userId, "", projectNameUpdate, projectDescriptionUpdate));
        @NotNull final Project newProject = new Project(projectName, projectDescription);
        projectService.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.updateById(null, projectId, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.updateById("", projectId, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.updateById(userId, projectId, null, projectDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.updateById(userId, projectId, "", projectDescriptionUpdate));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project updatedProject = projectService.updateById(userId, projectId, projectNameUpdate, projectDescriptionUpdate);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals(projectDescriptionUpdate, updatedProject.getDesc());
    }

    @Test
    @SneakyThrows
    public void updateByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> projectService.updateByIndex(userId, -1, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> projectService.updateByIndex(userId, projectService.getSize(userId), projectNameUpdate, projectDescriptionUpdate));
        @NotNull final Project newProject = new Project(projectName, projectDescription);
        projectService.add(userId, newProject);
        final int beforeSize = projectService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.updateByIndex(null, beforeSize - 1, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.updateByIndex("", beforeSize - 1, projectNameUpdate, projectDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.updateByIndex(userId, beforeSize - 1, null, projectDescriptionUpdate));
        Assert.assertThrows(NameIsEmptyException.class, () -> projectService.updateByIndex(userId, beforeSize - 1, "", projectDescriptionUpdate));
        @NotNull final Project updatedProject = projectService.updateByIndex(userId, beforeSize - 1, projectNameUpdate, projectDescriptionUpdate);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(projectNameUpdate, updatedProject.getName());
        Assert.assertEquals(projectDescriptionUpdate, updatedProject.getDesc());
    }

    @Test
    @SneakyThrows
    public void changeStatusByIdTest() {
        @NotNull final Project newProject = new Project(projectName, projectDescription);
        projectService.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.changeStatusById(null, projectId, newStatus));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.changeStatusById("", projectId, newStatus));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectService.changeStatusById(userId, null, newStatus));
        Assert.assertThrows(ProjectIdIsEmptyException.class, () -> projectService.changeStatusById(userId, "", newStatus));
        Assert.assertThrows(InvalidStatusException.class, () -> projectService.changeStatusById(userId, projectId, null));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project updatedProject = projectService.changeStatusById(userId, projectId, newStatus);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(newStatus, updatedProject.getStatus());
    }

    @Test
    @SneakyThrows
    public void changeStatusByIndexTest() {
        @NotNull final Project newProject = new Project(projectName, projectDescription);
        projectService.add(userId, newProject);
        final int beforeSize = projectService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.changeStatusByIndex(null, beforeSize - 1, newStatus));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.changeStatusByIndex("", beforeSize - 1, newStatus));
        Assert.assertThrows(NegativeIndexException.class, () -> projectService.changeStatusByIndex(userId, -1, newStatus));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> projectService.changeStatusByIndex(userId, beforeSize, newStatus));
        Assert.assertThrows(InvalidStatusException.class, () -> projectService.changeStatusByIndex(userId, beforeSize - 1, null));
        @NotNull final Project updatedProject = projectService.changeStatusByIndex(userId, beforeSize - 1, newStatus);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(0, difference);
        Assert.assertEquals(newStatus, updatedProject.getStatus());
    }

    @Test
    @SneakyThrows
    public void returnAllTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.returnAll(""));
        projectService.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectService.create(userId, name);
        }
        @NotNull final List<Project> currentProjectList = projectService.returnAll(userId);
        Assert.assertArrayEquals(currentProjectList.toArray(), expectedProjectList.toArray());
    }

    @Test
    @SneakyThrows
    public void returnAllWithSortTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.returnAll("", newSort.getComparator()));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.returnAll(null, newSort.getComparator()));
        projectService.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectService.create(userId, name);
        }
        @NotNull final List<Project> currentProjectList = projectService.returnAll(userId, newSort);
        expectedProjectList.sort(newSort.getComparator());
        Assert.assertArrayEquals(currentProjectList.toArray(), expectedProjectList.toArray());
    }

    @Test
    @SneakyThrows
    public void addTest() {
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.add(userId, null));
        @NotNull final Project newProject = new Project(projectName);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.add("", newProject));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.add(null, newProject));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project addedProject = projectService.add(userId, newProject);
        final int difference = projectService.getSize(userId) - beforeSize;
        Assert.assertEquals(1, difference);
        Assert.assertEquals(addedProject.getId(), newProject.getId());
        Assert.assertEquals(addedProject, newProject);
    }

    @Test
    @SneakyThrows
    public void clearTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.clear(""));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.clear(null));
        projectService.clear(userId);
        Assert.assertTrue(projectService.returnAll(userId).isEmpty());
    }

    @Test
    @SneakyThrows
    public void findByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> projectService.findById(userId, null));
        @NotNull final Project newProject = new Project(projectName);
        projectService.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findById("", projectId));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findById(null, projectId));
        final int beforeSize = projectService.getSize(userId);
        @NotNull final Project foundProject = projectService.findById(userId, projectId);
        Assert.assertEquals(beforeSize, projectService.getSize(userId));
        Assert.assertEquals(foundProject.getId(), projectId);
        Assert.assertEquals(foundProject, newProject);
    }

    @Test
    @SneakyThrows
    public void findByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> projectService.findByIndex(userId, -1));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> projectService.findByIndex(userId, projectService.getSize(userId)));
        @NotNull final Project newProject = new Project(projectName);
        projectService.add(userId, newProject);
        final int beforeSize = projectService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findByIndex("", beforeSize - 1));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findByIndex(null, beforeSize - 1));
        @NotNull final Project foundProject = projectService.findByIndex(userId, beforeSize - 1);
        Assert.assertEquals(beforeSize, projectService.getSize(userId));
        Assert.assertEquals(foundProject.getId(), newProject.getId());
        Assert.assertEquals(foundProject, newProject);
    }

    @Test
    @SneakyThrows
    public void removeByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> projectService.removeById(userId, null));
        @NotNull final Project newProject = new Project(projectName);
        projectService.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        final int beforeSize = projectService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.removeById("", projectId));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.removeById(null, projectId));
        @NotNull final Project removedProject = projectService.removeById(userId, projectId);
        final int difference = beforeSize - projectService.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.findById(userId, removedProject.getId()));
    }

    @Test
    @SneakyThrows
    public void removeByIndexTest() {
        Assert.assertThrows(NegativeIndexException.class, () -> projectService.removeByIndex(userId, -1));
        Assert.assertThrows(IndexOutOfSizeException.class, () -> projectService.removeByIndex(userId, projectService.getSize(userId)));
        @NotNull final Project newProject = new Project(projectName);
        projectService.add(userId, newProject);
        final int beforeSize = projectService.getSize(userId);
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findByIndex("", beforeSize - 1));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.findByIndex(null, beforeSize - 1));
        @NotNull final Project removedProject = projectService.removeByIndex(userId, beforeSize - 1);
        final int difference = beforeSize - projectService.getSize(userId);
        Assert.assertEquals(1, difference);
        Assert.assertThrows(EntityNotFoundException.class, () -> projectService.findById(userId, removedProject.getId()));
    }

    @Test
    @SneakyThrows
    public void getSizeTest() {
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.getSize(null));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.getSize(""));
        projectService.clear(userId);
        for (@NotNull final String name : projectsListName) {
            projectService.create(userId, name);
        }
        Assert.assertEquals(projectService.getSize(userId), expectedProjectList.size());
    }

    @Test
    @SneakyThrows
    public void existsByIdTest() {
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> projectService.existsById(userId, null));
        Assert.assertThrows(EntityIdIsEmptyException.class, () -> projectService.existsById(userId, ""));
        @NotNull final Project newProject = new Project(projectName);
        projectService.add(userId, newProject);
        @NotNull final String projectId = newProject.getId();
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.existsById(null, projectId));
        Assert.assertThrows(UserNotLoggedException.class, () -> projectService.existsById("", projectId));
        Assert.assertTrue(projectService.existsById(userId, projectId));
    }

}
