package ru.t1.dsinetsky.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.model.IWBS;
import ru.t1.dsinetsky.tm.enumerated.Status;

import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @Nullable
    private String desc = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull final String name) {
        this.name = name;
    }

    public Project(@NotNull final String name, @Nullable final String desc) {
        this.name = name;
        this.desc = desc;
    }

    @Override
    public String toString() {
        return new StringBuilder(name).append(" : ").append(desc).toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Project project = (Project) o;
        return name.equals(project.name) && Objects.equals(desc, project.desc) && status == project.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, desc, status);
    }

}
