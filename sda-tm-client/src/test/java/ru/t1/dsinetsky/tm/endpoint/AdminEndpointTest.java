package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserLockRequest;
import ru.t1.dsinetsky.tm.dto.request.user.admin.UserUnlockRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.marker.AdminIntegrationCategory;
import ru.t1.dsinetsky.tm.service.PropertyService;

import java.util.UUID;

@Category(AdminIntegrationCategory.class)
public class AdminEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAdminEndpoint adminEndpoint = IAdminEndpoint.newInstance();

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String invalidLogin = "InvalidLogin";

    @NotNull
    private final String userPassword = "Test";

    @NotNull
    private final String newUserLogin = "TestUser";

    @NotNull
    private final String newUserPassword = "TestPassword";

    @NotNull
    private final String adminLogin = "admin";

    @NotNull
    private final String adminPassword = "admin";

    @Nullable
    private String testToken;

    @NotNull
    private final String invalidToken = UUID.randomUUID().toString();

    @Test
    public void registryUserTest() {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(newUserLogin);
        request.setPassword(newUserPassword);
        @NotNull final UserRegistryResponse response = userEndpoint.registryUser(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(newUserLogin, response.getUser().getLogin());
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(null, newUserPassword)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest("", newUserPassword)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(newUserLogin, null)));
        Assert.assertThrows(Exception.class, () -> userEndpoint.registryUser(new UserRegistryRequest(newUserLogin, "")));
    }

    @Test
    public void lockUserByUserTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserLockRequest request = new UserLockRequest(testToken);
        request.setLogin(userLogin);
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(request));
    }

    @Test
    public void lockUserByAdminTest() {
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(adminLogin, adminPassword);
        testToken = authEndpoint.login(adminLoginRequest).getToken();
        @NotNull final UserLockRequest request = new UserLockRequest(testToken);
        request.setLogin(userLogin);
        adminEndpoint.lockUserByLogin(request);
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(new UserLockRequest()));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(new UserLockRequest(invalidToken)));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(new UserLockRequest(testToken, null)));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(new UserLockRequest(testToken, "")));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.lockUserByLogin(new UserLockRequest(testToken, invalidLogin)));
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(userLoginRequest));
    }

    @Test
    public void unlockUserByUserTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(testToken);
        request.setLogin(userLogin);
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(request));
    }

    @Test
    public void unlockUserByAdminTest() {
        @NotNull final UserLoginRequest adminLoginRequest = new UserLoginRequest(adminLogin, adminPassword);
        testToken = authEndpoint.login(adminLoginRequest).getToken();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(testToken);
        request.setLogin(userLogin);
        adminEndpoint.unlockUserByLogin(request);
        authEndpoint.logout(new UserLogoutRequest(testToken));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(new UserUnlockRequest()));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(new UserUnlockRequest(invalidToken)));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(new UserUnlockRequest(testToken, null)));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(new UserUnlockRequest(testToken, "")));
        Assert.assertThrows(Exception.class, () -> adminEndpoint.unlockUserByLogin(new UserUnlockRequest(testToken, invalidLogin)));
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        Assert.assertNotNull(authEndpoint.login(userLoginRequest).getToken());
    }

}
