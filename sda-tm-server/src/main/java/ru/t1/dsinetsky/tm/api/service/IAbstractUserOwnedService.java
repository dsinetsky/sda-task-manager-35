package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;
import ru.t1.dsinetsky.tm.model.User;

import java.util.List;

public interface IAbstractUserOwnedService<M extends AbstractUserOwnedModel> extends IAbstractUserOwnedRepository<M> {

    @NotNull
    List<M> returnAll(@Nullable String userId, @Nullable Sort sort) throws GeneralException;

    @NotNull
    M findById(@Nullable String userId, @Nullable String id) throws GeneralException;

    @NotNull
    M findByIndex(@Nullable String userId, int index) throws GeneralException;

    @NotNull
    M removeById(@Nullable String userId, @Nullable String id) throws GeneralException;

    @NotNull
    M removeByIndex(@Nullable String userId, int index) throws GeneralException;

    void createTest(@NotNull User user) throws GeneralException;

}
