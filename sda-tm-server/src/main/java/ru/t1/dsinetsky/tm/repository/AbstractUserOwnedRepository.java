package ru.t1.dsinetsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IAbstractUserOwnedRepository;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.AbstractUserOwnedModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IAbstractUserOwnedRepository<M> {

    @Override
    @NotNull
    public List<M> returnAll(@NotNull final String userId) {
        return models
                .stream()
                .filter(m -> Objects.equals(userId, m.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<M> returnAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @NotNull final List<M> sortedList = new ArrayList<>(returnAll(userId));
        sortedList.sort(comparator);
        return sortedList;
    }

    @Override
    public void clear(@NotNull final String userId) {
        models.removeAll(returnAll(userId));
    }

    @Override
    @Nullable
    public M findById(@NotNull final String userId, @NotNull final String id) throws GeneralException {
        return models
                .stream()
                .filter(m -> Objects.equals(userId, m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public M findByIndex(@NotNull final String userId, final int index) throws GeneralException {
        if (index < 0) return null;
        if (index >= getSize(userId)) return null;
        return returnAll(userId).get(index);
    }

    @Override
    @Nullable
    public M removeById(@NotNull final String userId, @NotNull final String id) throws GeneralException {
        @Nullable final M model = findById(userId, id);
        models.remove(model);
        return model;
    }

    @Override
    @NotNull
    public M removeByIndex(@NotNull final String userId, final int index) throws GeneralException {
        @Nullable final M model = findByIndex(userId, index);
        models.remove(model);
        return model;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws GeneralException {
        return models
                .stream()
                .anyMatch(m -> userId.equals(m.getUserId()) && id.equals(m.getId()));
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    @Override
    @NotNull
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        models.add(model);
        return model;
    }

}
