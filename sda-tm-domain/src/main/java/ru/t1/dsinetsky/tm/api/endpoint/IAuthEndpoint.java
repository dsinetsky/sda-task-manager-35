package ru.t1.dsinetsky.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserLoginResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserLogoutResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    @SneakyThrows
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    );

    @NotNull
    @WebMethod
    @SneakyThrows
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    @SneakyThrows
    UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    );

}
