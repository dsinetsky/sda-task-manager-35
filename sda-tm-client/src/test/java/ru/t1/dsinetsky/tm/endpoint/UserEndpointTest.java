package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.endpoint.IAdminEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IUserEndpoint;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.dto.request.user.*;
import ru.t1.dsinetsky.tm.dto.response.user.*;
import ru.t1.dsinetsky.tm.marker.IntegrationCategory;
import ru.t1.dsinetsky.tm.service.PropertyService;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.UUID;

@Category(IntegrationCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAdminEndpoint adminEndpoint = IAdminEndpoint.newInstance();

    @NotNull
    private final String userFirstName = "TestFirstName";

    @NotNull
    private final String userLastName = "TestLastName";

    @NotNull
    private final String userMiddleName = "TestMiddleName";

    @NotNull
    private final String userEmail = "TestUserEmail";

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String invalidLogin = "InvalidLogin";

    @NotNull
    private final String invalidPassword = "InvalidPassword";

    @NotNull
    private final String userPassword = "Test";

    @NotNull
    private final String newUserLogin = "TestUser";

    @NotNull
    private final String newUserPassword = "TestPassword";

    @Nullable
    private String testToken;

    @NotNull
    private final String invalidToken = UUID.randomUUID().toString();

    @Test
    public void updateCurrentUserTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserUpdateNameRequest request = new UserUpdateNameRequest(testToken);
        request.setFirstName(userFirstName);
        request.setLastName(userLastName);
        request.setMiddleName(userMiddleName);
        @NotNull final UserUpdateNameResponse response = userEndpoint.updateUserName(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(userFirstName, response.getUser().getFirstName());
        Assert.assertEquals(userLastName, response.getUser().getLastName());
        Assert.assertEquals(userMiddleName, response.getUser().getMiddleName());
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserName(new UserUpdateNameRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserName(new UserUpdateNameRequest(invalidToken)));
    }

    @Test
    public void updateCurrentUserEmailTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserUpdateEmailRequest request = new UserUpdateEmailRequest(testToken);
        request.setEmail(userEmail);
        @NotNull final UserUpdateEmailResponse response = userEndpoint.updateUserEmail(request);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(userEmail, response.getUser().getEmail());
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserEmail(new UserUpdateEmailRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.updateUserEmail(new UserUpdateEmailRequest(invalidToken)));
    }

    @Test
    public void changeUserPasswordPositiveTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(testToken);
        request.setPassword(newUserPassword);
        @NotNull final UserChangePasswordResponse response = userEndpoint.changeUserPassword(request);
        @Nullable final String hash = HashUtil.salt(propertyService, newUserPassword);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(hash, response.getUser().getPasswordHash());
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest()));
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(new UserChangePasswordRequest(invalidToken)));
        request.setPassword(userPassword);
        userEndpoint.changeUserPassword(request);
    }

    @Test
    public void changeUserPasswordEmptyPasswordTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(testToken);
        request.setPassword("");
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(request));
    }

    @Test
    public void changeUserPasswordNullPasswordTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(testToken);
        request.setPassword(null);
        Assert.assertThrows(Exception.class, () -> userEndpoint.changeUserPassword(request));
    }

    @Test
    public void loginUserTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(userLoginRequest);
        Assert.assertTrue(userLoginResponse.getSuccess());
        Assert.assertNotNull(userLoginResponse.getToken());
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(null, userPassword)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest("", userPassword)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(invalidLogin, userPassword)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(userLogin, null)));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(userLogin, "")));
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(new UserLoginRequest(userLogin, invalidPassword)));
    }

    @Test
    public void logoutUserTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(testToken);
        @NotNull final UserLogoutResponse response = authEndpoint.logout(request);
        Assert.assertTrue(response.getSuccess());
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.logout(new UserLogoutRequest(invalidToken)));
    }

    @Test
    public void viewUserProfileTest() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserViewProfileResponse response = authEndpoint.viewProfile(new UserViewProfileRequest(testToken));
        Assert.assertTrue(response.getSuccess());
        Assert.assertNotNull(response.getUser());
        Assert.assertThrows(Exception.class, () -> authEndpoint.viewProfile(new UserViewProfileRequest()));
        Assert.assertThrows(Exception.class, () -> authEndpoint.viewProfile(new UserViewProfileRequest(invalidToken)));
    }

}
