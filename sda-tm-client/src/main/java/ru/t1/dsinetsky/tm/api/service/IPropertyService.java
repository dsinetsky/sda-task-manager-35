package ru.t1.dsinetsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    int getPasswordIteration();

    @NotNull
    String getApplicationVersion();

    @NotNull String getPasswordSecret();
}
