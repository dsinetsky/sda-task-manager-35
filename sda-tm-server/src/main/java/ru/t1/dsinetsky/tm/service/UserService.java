package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.repository.IUserRepository;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.api.service.IUserService;
import ru.t1.dsinetsky.tm.builder.repository.UserBuilder;
import ru.t1.dsinetsky.tm.enumerated.Role;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.user.*;
import ru.t1.dsinetsky.tm.model.User;
import ru.t1.dsinetsky.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IUserRepository repository,
                       @NotNull final IProjectRepository projectRepository,
                       @NotNull final ITaskRepository taskRepository,
                       @NotNull final IPropertyService propertyService) {
        super(repository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.propertyService = propertyService;
    }

    @Override
    @NotNull
    public User add(@Nullable final User newUser) throws GeneralException {
        if (newUser == null) throw new InvalidUserException();
        @NotNull final String login = newUser.getLogin();
        if (login.isEmpty()) throw new IncorrectLoginPasswordException();
        if (isUserExistByLogin(login)) throw new UserAlreadyExistException();
        @NotNull final String password = newUser.getPasswordHash();
        if (password.isEmpty()) throw new IncorrectLoginPasswordException();
        newUser.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.add(newUser);
        return newUser;
    }

    @Override
    public void removeUserByLogin(@Nullable final String login) throws GeneralException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        @Nullable final User user = findUserByLogin(login);
        @NotNull final String userId = user.getId();
        projectRepository.clear(userId);
        taskRepository.clear(userId);
        repository.removeByLogin(login);
    }

    @Override
    public void clear() throws GeneralException {
        @Nullable final List<User> users = new ArrayList<>(returnAll());
        for (@NotNull final User user : users) {
            if (!user.getRole().equals(Role.ADMIN))
                removeUserByLogin(user.getLogin());
        }
    }

    @Override
    @NotNull
    public User findUserByLogin(@Nullable final String login) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        return repository.findUserByLogin(login);
    }

    @Override
    @NotNull
    public User updateUserById(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        @NotNull final User user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    @NotNull
    public User updateUserByLogin(@Nullable final String login, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = findUserByLogin(login);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    @NotNull
    public User updateEmailById(@Nullable final String id, @Nullable final String email) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        @NotNull final User user = findById(id);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User updateEmailByLogin(@Nullable final String login, @Nullable final String email) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        if (!isUserExistByLogin(login)) throw new UserNotFoundException();
        @NotNull final User user = findUserByLogin(login);
        user.setEmail(email);
        return user;
    }

    @Override
    @NotNull
    public User changePassword(@Nullable final String id, @Nullable final String password) throws GeneralException {
        if (id == null || id.isEmpty()) throw new UserIdIsEmptyException();
        if (password == null || password.isEmpty()) throw new UserPasswordIsEmptyException();
        if (!existsById(id)) throw new UserNotFoundException();
        @NotNull final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    public boolean isUserExistByLogin(@Nullable final String login) throws GeneralUserException {
        if (login == null || login.isEmpty()) throw new UserLoginIsEmptyException();
        return repository.isUserLoginExist(login);
    }

    @Override
    public void createTest() throws GeneralException {
        this.add(UserBuilder.create().login("admin").password("admin").role(Role.ADMIN).toUser());
        this.add(UserBuilder.create().login("Test").password("Test").toUser());
        this.add(UserBuilder.create().login("dsinetsky").password("change").email("dsinetsky@t1-consulting.ru").toUser());
        this.add(UserBuilder
                .create()
                .login("user")
                .password("pass")
                .email("user-email@gmail.com")
                .firstName("Ivan")
                .lastName("Ivanov")
                .middleName("Ivanovich")
                .role(Role.USUAL)
                .toUser()
        );
    }

}
