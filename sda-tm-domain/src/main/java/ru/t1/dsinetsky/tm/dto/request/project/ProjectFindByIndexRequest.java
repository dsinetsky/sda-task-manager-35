package ru.t1.dsinetsky.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dsinetsky.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectFindByIndexRequest extends AbstractUserRequest {

    private int index;

    public ProjectFindByIndexRequest(final @Nullable String token) {
        super(token);
    }

}
