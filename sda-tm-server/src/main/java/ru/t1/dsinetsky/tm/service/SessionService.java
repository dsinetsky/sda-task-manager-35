package ru.t1.dsinetsky.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.api.repository.ISessionRepository;
import ru.t1.dsinetsky.tm.api.service.ISessionService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.model.Session;
import ru.t1.dsinetsky.tm.model.User;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(final @NotNull ISessionRepository repository) {
        super(repository);
    }

    @Override
    public void createTest(@NotNull final User user) throws GeneralException {

    }

}
