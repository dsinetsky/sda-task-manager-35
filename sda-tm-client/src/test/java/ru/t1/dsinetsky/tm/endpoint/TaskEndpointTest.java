package ru.t1.dsinetsky.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dsinetsky.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dsinetsky.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dsinetsky.tm.api.service.IPropertyService;
import ru.t1.dsinetsky.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.dsinetsky.tm.dto.request.task.*;
import ru.t1.dsinetsky.tm.dto.request.user.UserLoginRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserLogoutRequest;
import ru.t1.dsinetsky.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.dsinetsky.tm.dto.response.task.*;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.marker.IntegrationCategory;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.service.PropertyService;

import java.util.UUID;

@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final String userLogin = "Test";

    @NotNull
    private final String userPassword = "Test";

    @NotNull
    private final String projectName = "New project";

    @NotNull
    private final String projectDescription = "New project";

    @NotNull
    private final String taskName = "New task";

    @NotNull
    private final String taskDescription = "New task";

    @NotNull
    private final String taskNameUpdate = "Update task";

    @NotNull
    private final String taskDescriptionUpdate = "Update task";

    @NotNull
    private final Status newStatus = Status.IN_PROGRESS;

    @NotNull
    private final Sort sort = Sort.BY_STATUS;

    @Nullable
    private String testToken;

    @NotNull
    private final String invalidToken = UUID.randomUUID().toString();

    @NotNull
    private final String invalidTaskId = UUID.randomUUID().toString();

    @NotNull
    private final String invalidProjectId = UUID.randomUUID().toString();

    @Before
    public void before() {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest(userLogin, userPassword);
        testToken = authEndpoint.login(userLoginRequest).getToken();
    }

    @After
    public void after() {
        taskEndpoint.clearTask(new TaskClearRequest(testToken));
        @NotNull final UserLogoutRequest userLogoutRequest = new UserLogoutRequest(testToken);
        authEndpoint.logout(userLogoutRequest);
    }

    @Test
    public void createTaskPositiveTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName(taskName);
        request.setDescription(taskDescription);
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        @Nullable final Task task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertEquals(taskName, task.getName());
        Assert.assertEquals(taskDescription, task.getDesc());
    }

    @Test
    public void createTaskNullNameTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName(null);
        request.setDescription(taskDescription);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void createTaskEmptyNameTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName("");
        request.setDescription(taskDescription);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void createTaskNullDescriptionTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName(taskName);
        request.setDescription(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void createTaskEmptyDescriptionTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(testToken);
        request.setName(taskName);
        request.setDescription("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void createTaskEmptyTokenTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest();
        request.setName(taskName);
        request.setDescription(taskDescription);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void createTaskInvalidTokenTest() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(invalidToken);
        request.setName(taskName);
        request.setDescription(taskDescription);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(request));
    }

    @Test
    public void clearTaskPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskClearResponse response = taskEndpoint.clearTask(new TaskClearRequest(testToken));
        Assert.assertNotNull(response);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        Assert.assertNull(taskListResponse.getTasks());
    }

    @Test
    public void clearTaskEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(new TaskClearRequest()));
    }

    @Test
    public void clearTaskInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(new TaskClearRequest(invalidToken)));
    }

    @Test
    public void changeTaskStatusByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setId(task.getId());
        request.setStatus(newStatus);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(newStatus, updatedTask.getStatus());
    }

    @Test
    public void changeTaskStatusByIdNullTaskIdTest() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setId(null);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIdEmptyTaskIdTest() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setId("");
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIdInvalidTaskIdTest() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setId(invalidTaskId);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIdNullStatusTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(testToken);
        request.setId(task.getId());
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIdEmptyTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest();
        request.setId(task.getId());
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIdInvalidTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(invalidToken);
        request.setId(task.getId());
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(request));
    }

    @Test
    public void changeTaskStatusByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(testToken);
        request.setIndex(size - 1);
        request.setStatus(newStatus);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(newStatus, updatedTask.getStatus());
    }

    @Test
    public void changeTaskStatusByIndexNullStatusTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(testToken);
        request.setIndex(size - 1);
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(request));
    }

    @Test
    public void changeTaskStatusByIndexNegativeIndexTest() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(testToken);
        request.setIndex(-1);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(request));
    }

    @Test
    public void changeTaskStatusByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(testToken);
        request.setIndex(size);
        request.setStatus(newStatus);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(request));
    }

    @Test
    public void changeTaskStatusByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest()));
    }

    @Test
    public void changeTaskStatusByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusByIndex(new TaskChangeStatusByIndexRequest(invalidToken)));
    }

    @Test
    public void completeTaskByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final TaskCompleteByIndexResponse response = taskEndpoint.completeTaskByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(Status.COMPLETED, updatedTask.getStatus());
    }

    @Test
    public void completeTaskByIndexNegativeIndexTest() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskByIndex(request));
    }

    @Test
    public void completeTaskByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskByIndex(request));
    }

    @Test
    public void completeTaskByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskByIndex(new TaskCompleteByIndexRequest()));
    }

    @Test
    public void completeTaskByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskByIndex(new TaskCompleteByIndexRequest(invalidToken)));
    }

    @Test
    public void startTaskByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final TaskStartByIndexResponse response = taskEndpoint.startTaskByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(Status.IN_PROGRESS, updatedTask.getStatus());
    }

    @Test
    public void startTaskByIndexNegativeIndexTest() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(request));
    }

    @Test
    public void startTaskByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(request));
    }

    @Test
    public void startTaskByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest()));
    }

    @Test
    public void startTaskByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskByIndex(new TaskStartByIndexRequest(invalidToken)));
    }

    @Test
    public void completeTaskByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(testToken);
        request.setId(task.getId());
        @NotNull final TaskCompleteByIdResponse response = taskEndpoint.completeTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(Status.COMPLETED, updatedTask.getStatus());
    }

    @Test
    public void completeTaskByIdNullTaskIdTest() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void completeTaskByIdEmptyTaskIdTest() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void completeTaskByIdInvalidTaskIdTest() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(testToken);
        request.setId(invalidTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void completeTaskByIdEmptyTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest();
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void completeTaskByIdInvalidTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(invalidToken);
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.completeTaskById(request));
    }

    @Test
    public void startTaskByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(testToken);
        request.setId(task.getId());
        @NotNull final TaskStartByIdResponse response = taskEndpoint.startTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(Status.IN_PROGRESS, updatedTask.getStatus());
    }

    @Test
    public void startTaskByIdNullTaskIdTest() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void startTaskByIdEmptyTaskIdTest() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void startTaskByIdInvalidTaskIdTest() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(testToken);
        request.setId(invalidTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void startTaskByIdEmptyTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest();
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void startTaskByIdInvalidTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(invalidToken);
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.startTaskById(request));
    }

    @Test
    public void findTaskByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskFindByIdRequest request = new TaskFindByIdRequest(testToken);
        request.setId(task.getId());
        @NotNull final TaskFindByIdResponse response = taskEndpoint.findTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task foundTask = response.getTask();
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task, foundTask);
        Assert.assertEquals(task.getId(), foundTask.getId());
    }

    @Test
    public void findTaskByIdNullTaskIdTest() {
        @NotNull final TaskFindByIdRequest request = new TaskFindByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(request));
    }

    @Test
    public void findTaskByIdEmptyTaskIdTest() {
        @NotNull final TaskFindByIdRequest request = new TaskFindByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(request));
    }

    @Test
    public void findTaskByIdInvalidTaskIdTest() {
        @NotNull final TaskFindByIdRequest request = new TaskFindByIdRequest(testToken);
        request.setId(invalidTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(request));
    }

    @Test
    public void findTaskByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(new TaskFindByIdRequest()));
    }

    @Test
    public void findTaskByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(new TaskFindByIdRequest(invalidToken)));
    }

    @Test
    public void findTaskByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskFindByIndexRequest request = new TaskFindByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final TaskFindByIndexResponse response = taskEndpoint.findTaskByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task foundTask = response.getTask();
        Assert.assertNotNull(foundTask);
        Assert.assertEquals(task, foundTask);
        Assert.assertEquals(task.getId(), foundTask.getId());
    }

    @Test
    public void findTaskByIndexNegativeIndexTest() {
        @NotNull final TaskFindByIndexRequest request = new TaskFindByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskByIndex(request));
    }

    @Test
    public void findTaskByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskFindByIndexRequest request = new TaskFindByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskByIndex(request));
    }

    @Test
    public void findTaskByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskByIndex(new TaskFindByIndexRequest()));
    }

    @Test
    public void findTaskByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskByIndex(new TaskFindByIndexRequest(invalidToken)));
    }

    @Test
    public void listTaskTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        Assert.assertNotNull(taskListResponse);
        Assert.assertFalse(taskListResponse.getTasks().isEmpty());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(new TaskListRequest(invalidToken)));
    }

    @Test
    public void listTaskSortPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListRequest request = new TaskListRequest(testToken);
        request.setSort(sort);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(request);
        Assert.assertNotNull(taskListResponse);
        Assert.assertFalse(taskListResponse.getTasks().isEmpty());
    }

    @Test
    public void listTaskNullSortTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListRequest request = new TaskListRequest(testToken);
        request.setSort(null);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(request);
        Assert.assertNotNull(taskListResponse);
        Assert.assertFalse(taskListResponse.getTasks().isEmpty());
    }

    @Test
    public void removeTaskByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(testToken);
        request.setId(task.getId());
        @NotNull final TaskRemoveByIdResponse response = taskEndpoint.removeTaskById(request);
        Assert.assertNotNull(response);
        @NotNull final TaskFindByIdRequest taskFindByIdRequest = new TaskFindByIdRequest(testToken);
        taskFindByIdRequest.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(taskFindByIdRequest));
    }

    @Test
    public void removeTaskByIdNullTaskIdTest() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(testToken);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(request));
    }

    @Test
    public void removeTaskByIdEmptyTaskIdTest() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(testToken);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(request));
    }

    @Test
    public void removeTaskByIdInvalidTaskIdTest() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(testToken);
        request.setId(invalidTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(request));
    }

    @Test
    public void removeTaskByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest()));
    }

    @Test
    public void removeTaskByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(new TaskRemoveByIdRequest(invalidToken)));
    }

    @Test
    public void removeTaskByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(testToken);
        request.setIndex(size - 1);
        @NotNull final TaskRemoveByIndexResponse response = taskEndpoint.removeTaskByIndex(request);
        Assert.assertNotNull(response);
        @NotNull final TaskFindByIdRequest taskFindByIdRequest = new TaskFindByIdRequest(testToken);
        taskFindByIdRequest.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.findTaskById(taskFindByIdRequest));
    }

    @Test
    public void removeTaskByIndexNegativeIndexTest() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(testToken);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(request));
    }

    @Test
    public void removeTaskByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(testToken);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(request));
    }

    @Test
    public void removeTaskByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest()));
    }

    @Test
    public void removeTaskByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskByIndex(new TaskRemoveByIndexRequest(invalidToken)));
    }

    @Test
    public void updateTaskByIdPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setId(task.getId());
        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(task.getId(), updatedTask.getId());
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals(taskDescriptionUpdate, updatedTask.getDesc());
    }

    @Test
    public void updateTaskByIdEmptyDescriptionTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(null);
        request.setId(task.getId());
        @NotNull final TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(task.getId(), updatedTask.getId());
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals("", updatedTask.getDesc());
    }

    @Test
    public void updateTaskByIdNullNameTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(null);
        request.setDescription(taskDescriptionUpdate);
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void updateTaskByIdEmptyNameTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        @Nullable final Task task = taskResponse.getTask();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName("");
        request.setDescription(taskDescriptionUpdate);
        request.setId(task.getId());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void updateTaskByIdNullTaskIdTest() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void updateTaskByIdEmptyTaskIdTest() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void updateTaskByIdInvalidTaskIdTest() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setId(invalidTaskId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(request));
    }

    @Test
    public void updateTaskByIdEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest()));
    }

    @Test
    public void updateTaskByIdInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(new TaskUpdateByIdRequest(invalidToken)));
    }

    @Test
    public void updateTaskByIndexPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setIndex(size - 1);
        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals(taskDescriptionUpdate, updatedTask.getDesc());
    }

    @Test
    public void updateTaskByIndexEmptyDescriptionTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(null);
        request.setIndex(size - 1);
        @NotNull final TaskUpdateByIndexResponse response = taskEndpoint.updateTaskByIndex(request);
        Assert.assertNotNull(response);
        @Nullable final Task updatedTask = response.getTask();
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(taskNameUpdate, updatedTask.getName());
        Assert.assertEquals("", updatedTask.getDesc());
    }

    @Test
    public void updateTaskByIndexNullNameTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @NotNull final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName(null);
        request.setDescription(taskDescriptionUpdate);
        request.setIndex(size - 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(request));
    }

    @Test
    public void updateTaskByIndexEmptyNameTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName("");
        request.setDescription(taskDescriptionUpdate);
        request.setIndex(size - 1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(request));
    }

    @Test
    public void updateTaskByIndexNegativeIndexTest() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setIndex(-1);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(request));
    }

    @Test
    public void updateTaskByIndexOutOfBoundIndexTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        taskEndpoint.createTask(taskRequest);
        @Nullable final TaskListResponse taskListResponse = taskEndpoint.listTask(new TaskListRequest(testToken));
        final int size = taskListResponse.getTasks().size();
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(testToken);
        request.setName(taskNameUpdate);
        request.setDescription(taskDescriptionUpdate);
        request.setIndex(size);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(request));
    }

    @Test
    public void updateTaskByIndexEmptyTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest()));
    }

    @Test
    public void updateTaskByIndexInvalidTokenTest() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskByIndex(new TaskUpdateByIndexRequest(invalidToken)));
    }

    @Test
    public void bindTaskToProjectPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(request);
        @NotNull final TaskFindByIdRequest boundTaskRequest = new TaskFindByIdRequest(testToken);
        boundTaskRequest.setId(taskId);
        @NotNull final TaskFindByIdResponse boundTaskResponse = taskEndpoint.findTaskById(boundTaskRequest);
        Assert.assertNotNull(boundTaskResponse.getTask());
        Assert.assertEquals(projectId, boundTaskResponse.getTask().getProjectId());
    }

    @Test
    public void bindTaskToProjectNullTaskTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(null);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectEmptyTaskTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId("");
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectInvalidTaskTest() {
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(invalidTaskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectNullProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectEmptyProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectInvalidProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectEmptyTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest();
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void bindTaskToProjectInvalidTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(invalidToken);
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(request));
    }

    @Test
    public void unbindTaskFromProjectPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        taskEndpoint.unbindTaskFromProject(request);
        @NotNull final TaskFindByIdRequest boundTaskRequest = new TaskFindByIdRequest(testToken);
        boundTaskRequest.setId(taskId);
        @NotNull final TaskFindByIdResponse boundTaskResponse = taskEndpoint.findTaskById(boundTaskRequest);
        Assert.assertNotNull(boundTaskResponse.getTask());
        Assert.assertNull(boundTaskResponse.getTask().getProjectId());
    }

    @Test
    public void unbindTaskFromProjectNullTaskTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(null);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectEmptyTaskTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId("");
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectInvalidTaskTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(invalidTaskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectNullProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectEmptyProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectInvalidProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(testToken);
        request.setTaskId(taskId);
        request.setProjectId(invalidProjectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectEmptyTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest();
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void unbindTaskFromProjectInvalidTokenTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(invalidToken);
        request.setTaskId(taskId);
        request.setProjectId(projectId);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(request));
    }

    @Test
    public void listTaskByProjectPositiveTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectRequest request = new TaskListByProjectRequest(testToken);
        request.setProjectId(projectId);
        @NotNull final TaskListByProjectResponse response = taskEndpoint.listTaskByProject(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getTasks().isEmpty());
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProject(new TaskListByProjectRequest()));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProject(new TaskListByProjectRequest(invalidToken)));
    }

    @Test
    public void listTaskByProjectNullProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectRequest request = new TaskListByProjectRequest(testToken);
        request.setProjectId(null);
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProject(request));
    }

    @Test
    public void listTaskByProjectEmptyProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectRequest request = new TaskListByProjectRequest(testToken);
        request.setProjectId("");
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProject(request));
    }

    @Test
    public void listTaskByProjectInvalidProjectTest() {
        @NotNull final TaskCreateRequest taskRequest = new TaskCreateRequest(testToken);
        taskRequest.setName(taskName);
        taskRequest.setDescription(taskDescription);
        @NotNull final TaskCreateResponse taskResponse = taskEndpoint.createTask(taskRequest);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @NotNull final ProjectCreateRequest projectRequest = new ProjectCreateRequest(testToken);
        projectRequest.setName(projectName);
        projectRequest.setDescription(projectDescription);
        @NotNull final ProjectCreateResponse projectResponse = projectEndpoint.createProject(projectRequest);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(testToken);
        bindRequest.setTaskId(taskId);
        bindRequest.setProjectId(projectId);
        taskEndpoint.bindTaskToProject(bindRequest);
        @NotNull final TaskListByProjectRequest request = new TaskListByProjectRequest(testToken);
        request.setProjectId(invalidProjectId);
        @NotNull final TaskListByProjectResponse response = taskEndpoint.listTaskByProject(request);
        Assert.assertNotNull(response);
        Assert.assertNull(response.getTasks());
    }

}