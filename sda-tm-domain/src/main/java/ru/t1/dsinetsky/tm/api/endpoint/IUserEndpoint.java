package ru.t1.dsinetsky.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.dsinetsky.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserRegistryRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateEmailRequest;
import ru.t1.dsinetsky.tm.dto.request.user.UserUpdateNameRequest;
import ru.t1.dsinetsky.tm.dto.response.user.UserChangePasswordResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserRegistryResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateEmailResponse;
import ru.t1.dsinetsky.tm.dto.response.user.UserUpdateNameResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    );

    @NotNull
    @WebMethod
    UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    );

    @NotNull
    @WebMethod
    UserUpdateEmailResponse updateUserEmail(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateEmailRequest request
    );

    @NotNull
    @WebMethod
    UserUpdateNameResponse updateUserName(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateNameRequest request
    );

}
